from PC.PenduloDuplo import PenduloDuplo as PD
from PC.MetodoPreditorCorretor import MetodoPreditorCorretor as MPC


# Equacao diferencial do problema
def function(t, y):
    return (y ** 2 + y) / t

# Equacao solucao do problema
def real_function(t):
    return (2 * t) / (1 - 2 * t)

preditor = MPC(function, real_function)
preditor.start()

pendulo = PD()
a = pendulo.start()