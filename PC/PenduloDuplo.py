from numpy import sin, cos, pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from PC.RungeKuttaSODE import RungeKuttaSODE




# Definicao das constantes

G = 9.789  # Aceleracao da gravidade em m/s^2

class PenduloDuplo:
    # Caracteristas do pendulo A
    rod_1 = 1.0  # comprimento em m
    mass_1 = 1.0  # massa em kg

    # Caracteristas do pendulo B
    rod_2 = 1.0  # comprimento em m
    mass_2 = 0.0  # massa em kg

    # Condicoes iniciais do pendulo A
    theta_1 = 120.0  # Angulo em graus
    omega_1 = 0.0  # Velocidade Angular em graus/segundo

    # Condicoes iniciais do pendulo A
    theta_2 = -10.0  # Angulo em graus
    omega_2 = 0.0  # Velocidade Angular em graus/segundo

    # Definindo as condicoes de tempo
    time_step = 0.05  # Passo de tempo em segundos
    init_time, final_time = 0, 100

    def __init__(self, rod_1=1.0, rod_2=1.0, mass_1=1.0, mass_2=1.0, theta_1=120.0, theta_2=-10.0, omega_1=0, omega_2=0, init_time=0, final_time=100, time_step=0.05):
        self.rod_1 = rod_1
        self.mass_1 = mass_1
        self.omega_1 = omega_1
        self.theta_1 = theta_1

        self.rod_2 = rod_2
        self.mass_2 = mass_2
        self.theta_2 = theta_2
        self.omega_2 = omega_2

        self.init_time = init_time
        self.final_time = final_time
        self.time_step = time_step

    def start(self):
        def equation_1(step, param):
            return param[1]

        def equation_2(step, param):
            theta_1, omega_1, theta_2, omega_2 = param
            return (-G * (2 * self.mass_1 + self.mass_2) * sin(theta_1) - self.mass_2 * G * sin(theta_1 - 2 * theta_2) - 2 * self.mass_2 * (omega_2 ** 2 * self.rod_2 + omega_1 ** 2 * self.rod_1 * cos(theta_1 - theta_2)) * sin(theta_1 - theta_2)) / \
                   (self.rod_1 * (2 * self.mass_1 + self.mass_2 - self.mass_2 * cos(2 * theta_1 - 2 * theta_2)))

        def equation_3(step, param):
            return param[3]

        def equation_4(step, param):
            theta_1, omega_1, theta_2, omega_2 = param
            return (2 * sin(theta_1 - theta_2) * (omega_1 ** 2 * self.rod_1 * (self.mass_1 + self.mass_2) + G * (self.mass_1 + self.mass_2) * cos(theta_1) + omega_2 ** 2 * self.rod_2 * self.mass_2 * cos(theta_1 - theta_2))) / \
                   (self.rod_2 * (2 * self.mass_1 + self.mass_2 - self.mass_2 * cos(2 * theta_1 - 2 * theta_2)))

        rk = RungeKuttaSODE(equation_1, equation_2, equation_3, equation_4)
        state = [np.array([self.theta_1, self.omega_1, self.theta_2, self.omega_2]) * (pi / 180)]
        t = self.init_time
        for step in range(1, int((self.final_time - self.init_time) / self.time_step)):
            state += [rk.calculate_at(t, state[step - 1], self.time_step)]
        state = np.array(state)

        x1 = self.rod_1 * sin(state[:, 0])
        y1 = -self.rod_1 * cos(state[:, 0])

        x2 = self.rod_2 * sin(state[:, 2]) + x1
        y2 = -self.rod_2 * cos(state[:, 2]) + y1

        fig = plt.figure()
        ax = fig.add_subplot(111, autoscale_on=False, xlim=[-(self.rod_1 + self.rod_2), (self.rod_1 + self.rod_2)], ylim=[-(self.rod_1 + self.rod_2), (self.rod_1 + self.rod_2)])
        ax.grid()

        line, = ax.plot([], [], 'k-', marker='o', markerfacecolor='r', markersize=20, linewidth=7)
        time = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        def init():
            line.set_data([], [])
            time.set_text('')
            return line, time

        def animate(i):
            massA = [0, x1[i], x2[i]]
            massB = [0, y1[i], y2[i]]

            line.set_data(massA, massB)
            time.set_text('time = %.1fs' % (i * self.time_step))
            return line, time

        fig = plt.gcf()
        fig.canvas.set_window_title('Runge-Kutta de Quarta Ordem para Sistemas')
        ani = animation.FuncAnimation(fig, animate, np.arange(1, len(state)), interval=25, blit=True, init_func=init)

        plt.axis('scaled')
        plt.ylim(-(self.rod_1 + self.rod_2), (self.rod_1 + self.rod_2))
        plt.xlim(-(self.rod_1 + self.rod_2), (self.rod_1 + self.rod_2))
        plt.show()
        return state
