class RungeKuttaODE:
    function = None

    def __init__(self, function):
        self.function = function

    def calculate_at(self, t, w, h):
        k = []
        k += [h * self.function(t, w)]
        k += [h * self.function(t + h / 2, w + h * k[0] / 2)]
        k += [h * self.function(t + h / 2, w + h * k[1] / 2)]
        k += [h * self.function(t + h, w + h * k[2])]
        return w + h * (k[0] + 2 * k[1] + 2 * k[2] + k[3]) / 6

