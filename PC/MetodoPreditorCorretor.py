import matplotlib.pyplot as plt
import numpy as np

from PC.RungeKuttaODE import RungeKuttaODE
from PC.AdamsBashforth import AdamsBashforth
from PC.AdamsMoulton import AdamsMoulton

class MetodoPreditorCorretor:
    a, b, h, steps, y0 = 1, 10, 0.01, 4, -2
    function, real_function = None, None

    def __init__(self, function, real_function, a=1, b=10, h=0.01, steps=4, y0=-2):
        self.function = function
        self.real_function = real_function
        self.a = a
        self.b = b
        self.h = h
        self.steps = steps
        self.y0 = y0

    def start(self):
        # Gerando os solucionadores dos metodos em questao
        rk, ab, am = RungeKuttaODE(self.function), AdamsBashforth(self.function), AdamsMoulton(self.function)

        # Criando as N-1 aproximacoes nessarias para o metodo
        w, t, y = [self.y0], [self.a], [self.y0]
        while len(w) < self.steps:
            w += [rk.calculate_at(t[len(t) - 1], w[len(w) - 1], self.h)]
            t += [t[len(t) - 1] + self.h]
            y += [self.real_function(t[len(t) - 1])]

        # Utilizando o Metodo Preditor-Corretor
        while t[len(t) - 1] <= self.b:
            w += [ab.calculate_at(t[len(t) - self.steps:], w[len(w) - self.steps:], self.h)]
            t += [t[len(t) - 1] + self.h]
            y += [self.real_function(t[len(t) - 1])]
            w[len(w) - 1] = am.calculate_at(t[len(t) - self.steps:], w[len(w) - self.steps:], self.h)

        #Configurando a visualização
        fig = plt.gcf()
        fig.canvas.set_window_title('Método Predito-Corretor de Quarta Ordem')
        plt.xlabel('Tempo')
        plt.ylabel('Valor')
        plt.plot(t, y, 'r--', t, w, 'b-')
        plt.xticks(np.arange(self.a, self.b))
        plt.grid(True, which='both')


        #Configurando escala e legenda
        xMin, xMax, yMin, yMax = t[0] - 2 * self.h, t[len(t) - 1], y[0], y[len(y) - 1] + 2 * self.h
        plt.ylim(yMin, yMax)
        plt.xlim(xMin, xMax)
        axes = fig.add_axes([xMin, xMax, yMin, yMax])
        solucaoAnalitica, solucaoAproximada = axes.plot(t, y, 'r--', t, w, 'b-')

        fig.legend([solucaoAproximada, solucaoAnalitica], ('Solução Aproximada', 'Solução Analítica'), 'upper right')

        #Exibindo visualizacao
        plt.show()
