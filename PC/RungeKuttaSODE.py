import numpy as np

class RungeKuttaSODE:
    functions = []

    def __init__(self, *functions):
        self.functions = functions

    def calculate_at(self, t, w, h):
        k = [[], [], [], []]
        w = np.array(w)

        for f in self.functions:
            k[0] += [h * f(t, w)]
        k[0] = np.array(k[0])

        for f in self.functions:
            k[1] += [h * f(t + h / 2, w + k[0] / 2)]
        k[1] = np.array(k[1])

        for f in self.functions:
            k[2] += [h * f(t + h / 2, w + k[1] / 2)]
        k[2] = np.array(k[2])

        for f in self.functions:
            k[3] += [h * f(t + h, w + k[2])]
        k[3] = np.array(k[3])

        return w + (k[0] + 2 * k[1] + 2 * k[2] + k[3]) / 6
