class AdamsBashforth:
    funcao = None

    def __init__(self, funcao):
        self.funcao = funcao

    def calculate_at(self, t, w, h):
        return w[3] + h * (55 * self.funcao(t[3], w[3]) - 59 * self.funcao(t[2], w[2]) + 37 * self.funcao(t[1], w[1]) - 9 * self.funcao(t[0], w[0])) / 24