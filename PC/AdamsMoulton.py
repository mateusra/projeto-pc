class AdamsMoulton:
    funcao = None

    def __init__(self, funcao):
        self.funcao = funcao

    def calculate_at(self, t, w, h):
        return w[2] + h * (9 * self.funcao(t[3], w[3]) + 19 * self.funcao(t[2], w[2]) - 5 * self.funcao(t[1], w[1]) + self.funcao(t[0], w[0])) / 24